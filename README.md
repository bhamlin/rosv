# RoSV - Rows of Serial Values implementation for Rust

I didn't really like the version included in the
[RSV Challenge repo](https://github.com/Stenway/RSV-Challenge), so I made my
own.

This crate adds functionality to `Vec<Vec<Option<String>>>` and
`Vec<Vec<Option<&str>>>` for serialization, and to `Vec<u8>` for
deserialization. Bare functions for serialization and deserialization are
available in `rosv::serialization` as well.

```rust
use rosv::{DeserializeRoSV, SerializeRoSV};

// Sample data
let rows = vec![
    vec![Some("Hello"), Some("🌎"), None, Some("")],
    vec![Some("A\0B\nC"), Some("Test 𝄞")],
    vec![],
    vec![Some("")],
];

// Return vec as a u8 stream readable by the rosv deserializer.
let stream = rows.serialize_rosv();

// Consume a stream and deserialize to a new vec.
let data = stream.deserialize_rosv().expect("Bad utf-8 in stream");
```
