pub mod error;

use self::error::RoSVFileError;
use crate::{DeserializeRoSV, RoSVContainer};
use error_stack::{Result, ResultExt};
use std::{
    fs::{File, OpenOptions},
    io::{BufReader, BufWriter, Error, Read, Write},
    path::Path,
};

/// Helper trait to read streams from a file.
pub trait RoSVReader {
    /// Read and deserialize stream from specified file.
    fn read_rosv_from<P: AsRef<Path>>(path: P) -> Result<RoSVContainer, RoSVFileError>;
}

impl RoSVReader for RoSVContainer {
    fn read_rosv_from<P: AsRef<Path>>(path: P) -> Result<RoSVContainer, RoSVFileError> {
        read_rosv_from(path)
    }
}

/// Helper trait to write streams to a file.
pub trait RoSVWriter {
    /// Clobbers existing file if present, and writes stream into file.
    fn write_to_file<P: AsRef<Path>>(&self, path: P) -> Result<usize, RoSVFileError>;
    /// Appends stream into file. Creates file if it doesn't already exist.
    fn append_to_file<P: AsRef<Path>>(&self, path: P) -> Result<usize, RoSVFileError>;
}

impl RoSVWriter for Vec<u8> {
    fn write_to_file<P: AsRef<Path>>(&self, path: P) -> Result<usize, RoSVFileError> {
        write_to_file(self, path)
    }

    fn append_to_file<P: AsRef<Path>>(&self, path: P) -> Result<usize, RoSVFileError> {
        append_to_file(self, path)
    }
}

/// Read and deserialize stream from specified file.
pub fn read_rosv_from<P: AsRef<Path>>(path: P) -> Result<RoSVContainer, RoSVFileError> {
    let (file_size, mut file_reader) =
        prepare_read(path).change_context_lazy(|| RoSVFileError::IoReadError)?;
    let mut buf = vec![0; file_size];
    file_reader
        .read_exact(&mut buf)
        .change_context_lazy(|| RoSVFileError::IoReadError)?;

    buf.deserialize_rosv()
        .change_context_lazy(|| RoSVFileError::DecodingError)
}

fn prepare_read<P: AsRef<Path>>(path: P) -> Result<(usize, BufReader<File>), Error> {
    let file_handle = OpenOptions::new().read(true).create(false).open(path)?;
    let file_size = file_handle.metadata()?.len() as usize;
    let file_reader = BufReader::new(file_handle);

    Ok((file_size, file_reader))
}

/// Clobbers existing file if present, and writes stream into file.
pub fn write_to_file<P: AsRef<Path>>(buf: &[u8], path: P) -> Result<usize, RoSVFileError> {
    let file_handle = File::create(path).change_context_lazy(|| RoSVFileError::IoWriteError)?;
    do_write(buf, file_handle)
}

/// Appends stream into file. Creates file if it doesn't already exist.
pub fn append_to_file<P: AsRef<Path>>(buf: &[u8], path: P) -> Result<usize, RoSVFileError> {
    let file_handle = OpenOptions::new()
        .write(true)
        .append(true)
        .create(true)
        .open(path)
        .change_context_lazy(|| RoSVFileError::IoWriteError)?;
    do_write(buf, file_handle)
}

fn do_write(buf: &[u8], file_handle: File) -> Result<usize, RoSVFileError> {
    let mut file_writer = BufWriter::new(file_handle);
    let bytes_written = file_writer
        .write(buf)
        .change_context_lazy(|| RoSVFileError::IoWriteError)?;

    Ok(bytes_written)
}
