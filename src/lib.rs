#[cfg(feature = "fs")]
pub mod fileio;
pub mod serialization;

use serialization::{deserialize_rosv, serialize_rosv};
use std::{rc::Rc, string::FromUtf8Error, sync::Arc};

pub const ROSV_FIELD_END: u8 = 0xff;
pub const ROSV_FIELD_NULL: u8 = 0xfe;
pub const ROSV_ROW_END: u8 = 0xfd;

pub type RoSVContainer = Vec<Vec<Option<String>>>;
pub type RoSVBase<Inner> = Vec<Vec<Option<Inner>>>;

/// Helper trait for RoSV Deserialization
pub trait DeserializeRoSV {
    /// Consume stream to output (probably) a RoSVContainer.
    /// Outputs Strings because everything is utf-8 for some reason.
    fn deserialize_rosv(self) -> Result<RoSVContainer, FromUtf8Error>;
}

impl DeserializeRoSV for Vec<u8> {
    fn deserialize_rosv(self) -> Result<RoSVContainer, FromUtf8Error> {
        deserialize_rosv(self)
    }
}

/// Helper trait for RoSV Serialization
pub trait SerializeRoSV {
    /// Read iterable to u8 stream.
    fn serialize_rosv(&self) -> Vec<u8>;
}

impl<Inner: SerializeToU8> SerializeRoSV for RoSVBase<Inner> {
    fn serialize_rosv(&self) -> Vec<u8> {
        serialize_rosv(self)
    }
}

/// Marker trait for types
pub trait SerializeToU8: Sized {
    fn to_u8(&self) -> Vec<u8>;
}

impl SerializeToU8 for String {
    fn to_u8(&self) -> Vec<u8> {
        self.clone().into_bytes()
    }
}

impl SerializeToU8 for &str {
    fn to_u8(&self) -> Vec<u8> {
        self.as_bytes().to_vec()
    }
}

impl SerializeToU8 for Rc<str> {
    fn to_u8(&self) -> Vec<u8> {
        self.as_bytes().to_vec()
    }
}

impl SerializeToU8 for Arc<str> {
    fn to_u8(&self) -> Vec<u8> {
        self.as_bytes().to_vec()
    }
}
