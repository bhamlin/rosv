use error_stack::Context;
use std::fmt::Display;

#[derive(Debug)]
pub enum RoSVFileError {
    IoReadError,
    IoWriteError,
    DecodingError,
    EncodingError,
}

impl Context for RoSVFileError {}
impl Display for RoSVFileError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let message = match self {
            Self::IoReadError => "Error reading from file",
            Self::IoWriteError => "Error writing to file",
            Self::DecodingError => "Error decoding rsv stream",
            Self::EncodingError => "Error encoding rsv stream",
        };

        write!(f, "{message}")
    }
}
