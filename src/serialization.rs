use crate::{RoSVContainer, SerializeToU8, ROSV_FIELD_END, ROSV_FIELD_NULL, ROSV_ROW_END};
use std::string::FromUtf8Error;

/// Consume stream to output (probably) a RoSVContainer.
/// Outputs Strings because everything is utf-8 for some reason.
pub fn serialize_rosv<'a, Inner, IterValues, IterRows>(data: IterRows) -> Vec<u8>
where
    Inner: SerializeToU8 + 'a,
    IterValues: IntoIterator<Item = &'a Option<Inner>>,
    IterRows: IntoIterator<Item = IterValues>,
{
    let mut stream = Vec::default();

    for datum in data {
        stream.extend(serialize_to_rosv_row(datum));
    }

    stream
}

fn serialize_to_rosv_row<'a, Inner, Iterable>(data: Iterable) -> Vec<u8>
where
    Inner: SerializeToU8 + 'a,
    Iterable: IntoIterator<Item = &'a Option<Inner>>,
{
    let mut stream = Vec::default();

    for datum in data {
        match datum {
            Some(chunk) => {
                stream.extend(chunk.to_u8());
                stream.push(ROSV_FIELD_END);
            }
            None => {
                stream.push(ROSV_FIELD_NULL);
            }
        }
    }

    stream.push(ROSV_ROW_END);

    stream
}

/// Read iterable to u8 stream.
pub fn deserialize_rosv<IterRows>(stream: IterRows) -> Result<RoSVContainer, FromUtf8Error>
where
    IterRows: IntoIterator<Item = u8>,
{
    let mut output: Vec<Vec<Option<String>>> = RoSVContainer::new();
    let mut row = Vec::new();
    let mut field = Vec::new();

    for byte in stream {
        match byte {
            ROSV_FIELD_END => {
                row.push(Some(String::from_utf8(field)?));
                field = Vec::new();
            }
            ROSV_FIELD_NULL => {
                row.push(None);
            }
            ROSV_ROW_END => {
                output.push(row);
                row = Vec::new();
            }
            b => field.push(b),
        }
    }

    Ok(output)
}
