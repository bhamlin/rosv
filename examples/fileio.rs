use clap::Parser;
use rosv::fileio::{RoSVReader, RoSVWriter};
use rosv::{RoSVContainer, SerializeRoSV};
use std::fs;
use std::path::PathBuf;

fn main() {
    let rows = vec![
        vec![Some("Hello"), Some("🌎"), None, Some("")],
        vec![Some("A\0B\nC"), Some("Test 𝄞")],
        vec![],
        vec![Some("")],
    ];

    let opts = CommandLineOptions::parse();
    let path = opts.temp_file;

    println!("{rows:?}");
    let stream = rows.serialize_rosv();
    println!("{stream:?} {}/{}", stream.len(), stream.capacity());

    let byte_count = stream
        .write_to_file(&path)
        .expect("Error writing stream to file");
    println!("Wrote {byte_count} bytes to {}", path.display());

    let byte_count = stream
        .append_to_file(&path)
        .expect("Error writing stream to file");
    println!("Wrote {byte_count} bytes appended to {}", path.display());

    let data = RoSVContainer::read_rosv_from(&path).expect("Error reading or parsing file");
    println!("{data:?}");

    fs::remove_file(&path).expect("Unable to delete temporary file");
}

#[derive(Parser)]
struct CommandLineOptions {
    #[clap(long, default_value = "/tmp/temp.rsv")]
    pub temp_file: PathBuf,
}
