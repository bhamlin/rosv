use rosv::{DeserializeRoSV, SerializeRoSV};

fn main() {
    let rows = vec![
        vec![Some("Hello"), Some("🌎"), None, Some("")],
        vec![Some("A\0B\nC"), Some("Test 𝄞")],
        vec![],
        vec![Some("")],
    ];

    println!("{rows:?}");
    let stream = rows.serialize_rosv();
    println!("{stream:?} {}/{}", stream.len(), stream.capacity());
    let data = stream.deserialize_rosv().expect("Bad utf-8 in stream");
    println!("{data:?}");
}
